package com.teddy.photoapp.api.users.shared;

import java.io.Serializable;
import java.util.List;

import com.teddy.photoapp.api.users.ui.model.AlbumResponseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8352899468672651510L;
	
	private String firstName;
	private String lastName;
	private String password;
	private String email;
	private String userId;
	private String encryptedPassword;
	private List<AlbumResponseModel> albums;
	
}
