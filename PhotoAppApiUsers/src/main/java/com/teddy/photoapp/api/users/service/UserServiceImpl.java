package com.teddy.photoapp.api.users.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.teddy.photoapp.api.users.data.AlbumsServiceClient;
import com.teddy.photoapp.api.users.data.UserEntity;
import com.teddy.photoapp.api.users.data.UserRepository;
import com.teddy.photoapp.api.users.shared.UserDto;
import com.teddy.photoapp.api.users.ui.model.AlbumResponseModel;

import feign.FeignException;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	Environment environment;
	
	@Autowired
	AlbumsServiceClient albumsServiceClient;
	
	@Override
	public UserDto createUser(UserDto userDetails) {
		userDetails.setUserId(UUID.randomUUID().toString());
		userDetails.setEncryptedPassword(bCryptPasswordEncoder.encode(userDetails.getPassword()));
		
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		
		UserEntity user = modelMapper.map(userDetails, UserEntity.class);
		userRepository.save(user);
		
		UserDto returnedValue = modelMapper.map(user, UserDto.class);
		return returnedValue;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByEmail(username);
		if(userEntity == null) {
			throw new UsernameNotFoundException(username);
		}
		return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), true, true, true, true, new ArrayList<>());
	}

	@Override
	public UserDto getUserDetailsByEmail(String email) {
		UserEntity userEntity = userRepository.findByEmail(email);
		if(userEntity == null) {
			throw new UsernameNotFoundException(email);
		}
		return new ModelMapper().map(userEntity, UserDto.class);
	}

	@Override
	public UserDto getUserByUserId(String userId) {
		UserEntity userEntity = userRepository.findByUserId(userId);
		if(userEntity == null) {
			throw new UsernameNotFoundException("User not found");
		}
		logger.info("Before calling albums microservice");
		String albumUrl = String.format(environment.getProperty("album.url"), userId);
		ResponseEntity<List<AlbumResponseModel>> albumsResponse = restTemplate.exchange(albumUrl, HttpMethod.GET, null, new ParameterizedTypeReference<List<AlbumResponseModel>>() {});
		List<AlbumResponseModel> albums = albumsResponse.getBody();
		/*List<AlbumResponseModel> albums = albumsServiceClient.getAlbums(userId);*/
		logger.info("After calling albums microservice");
		UserDto userDto = new ModelMapper().map(userEntity, UserDto.class);
		userDto.setAlbums(albums);
		return userDto;
	}

}
