package com.teddy.photoapp.api.gateway.zuulfilter;

import java.util.Enumeration;

import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

public class PreFilter extends ZuulFilter {

	@Override
	public boolean shouldFilter() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object run() throws ZuulException {
		RequestContext ctx = RequestContext.getCurrentContext();
		StringBuffer strLog = new StringBuffer();
		strLog.append("\n------ NUEVA PETICION ------\n");
		strLog.append(String.format("Server: %s Metodo: %s Path: %s \n", ctx.getRequest().getServerName(),
				ctx.getRequest().getMethod(), ctx.getRequest().getRequestURI()));
		Enumeration<String> enume = ctx.getRequest().getHeaderNames();
		String header;
		while (enume.hasMoreElements()) {
			header = enume.nextElement();
			strLog.append(String.format("Headers: %s = %s \n", header, ctx.getRequest().getHeader(header)));
		}
		;
		System.out.println(strLog.toString());
		return null;
	}

	@Override
	public String filterType() {
		// TODO Auto-generated method stub
		return "pre";
	}

	@Override
	public int filterOrder() {
		// TODO Auto-generated method stub
		return FilterConstants.SEND_RESPONSE_FILTER_ORDER;
	}

}
